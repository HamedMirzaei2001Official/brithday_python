import pyfiglet
import datetime

class BirthDay:
    @classmethod
    def get_day(cls, time):
        return str(time)[8] + str(time)[9]

    @classmethod
    def get_month(cls, time):
        return str(time)[6]

    @classmethod
    def check_date(cls, time):
        DATE = {'day': '31', 'month': '8', 'year': '2001'}
        if DATE['day'] == time['day'] and DATE['month'] == time['month']:
            pyfiglet.print_figlet(text="h a p p y    b i r t h\nd a y"
                                  , font='standard', colors='LIGHT_CYAN')



if __name__ == "__main__":
    time = datetime.datetime.now()
    day = BirthDay.get_day(time)
    month = BirthDay.get_month(time)
    BirthDay.check_date({'day': day, 'month': month})